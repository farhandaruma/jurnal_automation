from main import *
import traceback
import csv
import json


def purchase(data):



    daruma_address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."

    due_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                             data['invoices'][0]['invoice_date'][5:7],
                             data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else "%s/%s/%s" % (datetime.datetime.now().day, datetime.datetime.now().month, datetime.datetime.now().year)
    shipping_date = "%s/%s/%s" % (data['deliveries'][0]['delivery_date'][8:10],
                                  data['deliveries'][0]['delivery_date'][5:7],
                                  data['deliveries'][0]['delivery_date'][:4]) if len(data['deliveries']) > 0 else None
    shipping_price = data['invoices'][0]['shipping_fee'] if len(data['invoices']) > 0 else None
    shipping_address = data['deliveries'][0]['ship_to'] if len(data['deliveries']) > 0 and data['deliveries'][0]['ship_to'] is not None else daruma_address
    paid_date = "%s/%s/%s" % (data['payments'][0]['paid_at'][8:10],
                              data['payments'][0]['paid_at'][5:7],
                              data['payments'][0]['paid_at'][:4]) if len(data['payments']) > 0 else None
    ship_via = data['shipping_method'] if data['shipping_method'] is not 'None' else None
    tracking_no = None
    address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
    term_name = data['payment_term']
    transaction_date_po = "%s/%s/%s" % (data['order_date'][8:10],
                                        data['order_date'][5:7],
                                        data['order_date'][:4])
    deposit = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else 0
    discount_unit = None
    person_name = "%s %s".strip() % (data['contact']['first_name'], data['contact']['last_name'])
    warehouse_name = "Gudang Garam"
    email = None
    paid_amount = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else None
    transaction_no = data['order_no']
    memo = None
    products = data['lines']
    payment_method = data['payments'][0]['payment_method'] if len(data['payments']) > 0 else "Cash"
    invoice_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                                 data['invoices'][0]['invoice_date'][5:7],
                                 data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else None

    print("creating purchase for transaction no: %s" % transaction_no)



    total_transaction = data['total_incl_tax']

    purchase_order_response = dict(
        order_no=transaction_no,
        purchase_order_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )



    transaction_response = dict(
        order_no=transaction_no,
        purchase_order=purchase_order_response,
        deliveries=[],
        invoices=[],
        payments=[]
    )

    purchase_order = create_purchase_order(transaction_date=transaction_date_po,
                                           shipping_date=shipping_date,
                                           shipping_price=shipping_price,
                                           shipping_address=shipping_address,
                                           ship_via=ship_via,
                                           tracking_no=tracking_no,
                                           address=address,
                                           term_name=term_name,
                                           due_date=due_date,
                                           deposit=deposit,
                                           discount_unit=discount_unit,
                                           person_name=person_name,
                                           warehouse_name=warehouse_name,
                                           email=email,
                                           transaction_no=transaction_no,
                                           memo=memo,
                                           products=products,
                                           total_transaction=total_transaction)
    if purchase_order.get("transaction_no", None):
        purchase_order_response['message'] = 'exists'
        # pprint.pprint(purchase_order)
        if not data['deliveries']:
            print('deliveries data not available')
            return transaction_response
        elif data['deliveries'] and len(purchase_order.get('deliveries')) == 0:
            deliveries = data['deliveries']
            invoices = data['invoices']

            del_and_inv = []
            if len(del_and_inv) == 0:
                print('deliveries data not available')
                return transaction_response

            print("creating delivery and invoice order")
            for x in zip(deliveries, invoices):
                del_and_inv.append(x)

            deliveries_list = transaction_response['deliveries']
            invoices_list = transaction_response['invoices']
            for task in del_and_inv:

                delivery_order_response = dict(
                    order_no=transaction_no,
                    delivery_order_id=None,
                    message=None,
                    error_message=None,
                    jurnal_message=None
                )

                invoice_order_response = dict(
                    order_no=transaction_no,
                    invoice_order_id=None,
                    message=None,
                    error_message=None,
                    jurnal_message=None
                )

                print("creating %s" % task[0]['id'])
                shipping_date = "%s/%s/%s" % (task[0]['delivery_date'][8:10],
                                              task[0]['delivery_date'][5:7],
                                              task[0]['delivery_date'][:4])
                delivery_no = task[0]['delivery_no']
                shipping_address = task[0]['ship_to'] if task[0]['ship_to'] is not None else daruma_address
                selected_po_id = get_purchase_order_id_by_transaction_no(transaction_no=transaction_no)
                purchase_delivery = create_purchase_delivery(person_name=person_name,
                                                             shipping_address=shipping_address,
                                                             transaction_date=shipping_date,
                                                             ship_via=ship_via,
                                                             tracking_no=tracking_no,
                                                             transaction_no=transaction_no,
                                                             reference_no=None,
                                                             selected_po_id=selected_po_id,
                                                             products=products,
                                                             shipping_price=shipping_price,
                                                             memo=memo,
                                                             message=None,
                                                             custom_id=delivery_no)

                # pprint.pprint(purchase_delivery)
                if not purchase_delivery:
                    print("unknown error")
                    return transaction_response
                while purchase_delivery.get("error_full_messages", None):

                    selected_po_id = get_purchase_order_id_by_transaction_no(transaction_no=transaction_no)

                    purchase_delivery = create_purchase_delivery(person_name=person_name,
                                                                 shipping_address=shipping_address,
                                                                 transaction_date=shipping_date,
                                                                 ship_via=ship_via,
                                                                 tracking_no=tracking_no,
                                                                 transaction_no=transaction_no,
                                                                 reference_no=None,
                                                                 selected_po_id=selected_po_id,
                                                                 products=products,
                                                                 shipping_price=shipping_price,
                                                                 memo=memo,
                                                                 message=None,
                                                                 custom_id=delivery_no)
                    # sync with purchase delivery

                    # pprint.pprint(purchase_delivery)
                    if purchase_delivery.get('product di transaction_lines_attributes untuk baris ke-1', None):
                        delivery_order_response['jurnal_message'] = purchase_delivery.get('product di transaction_lines_attributes untuk baris ke-1', None)
                        deliveries_list.append(delivery_order_response)
                        return transaction_response

                delivery_order_response['message'] = 'created'

                purchase_delivery_id = purchase_delivery['purchase_delivery']['id']

                delivery_order_response['delivery_order_id'] = purchase_delivery_id

                deliveries_list.append(delivery_order_response)

                invoice_date = "%s/%s/%s" % (task[1]['invoice_date'][8:10],
                                             task[1]['invoice_date'][5:7],
                                             task[1]['invoice_date'][:4])

                purchase_invoice = convert_to_purchase_invoice(id=purchase_delivery_id,
                                                               invoice_date=invoice_date)



                # pprint.pprint(purchase_invoice)

                invoice_order_response['message'] = 'created'
                invoice_order_response['invoice_order_id'] = purchase_invoice['purchase_invoice']['id']
                invoices_list.append(invoice_order_response)

                if str(term_name) != 'CBD':
                    print("this is not CBD")
                    payments = data['payments']
                    if payments:

                        payment_order_response = dict(
                            order_no=transaction_no,
                            purchase_id=None,
                            message=None,
                            error_message=None,
                            jurnal_message=None
                        )

                        purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
                                                                              transaction_no=transaction_no,
                                                                              transaction_date=paid_date,
                                                                              purchase_id=purchase_invoice[
                                                                                  'purchase_invoice'][
                                                                                  'transaction_no'],
                                                                              amount_receive_payment=paid_amount,
                                                                              payment_method_name=payment_method)

                        # pprint.pprint(purchase_order_payment)
                        payment_order_response['purchase_id'] = purchase_invoice['purchase_invoice']['transaction_no']
                        payment_res = transaction_response['payments']
                        payment_res.append(payment_order_response)
                        print("creating purchase invoice 1")
                        remaining_currency = purchase_invoice['purchase_invoice']['remaining']

                        if float(remaining_currency) > 0.0:
                            payment_order_response_2 = dict(
                                order_no=transaction_no,
                                purchase_id=None,
                                message=None,
                                error_message=None,
                                jurnal_message=None
                            )
                            purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
                                                                                  transaction_no=transaction_no,
                                                                                  transaction_date=paid_date,
                                                                                  purchase_id=purchase_invoice[
                                                                                      'purchase_invoice'][
                                                                                      'transaction_no'],
                                                                                  amount_receive_payment=remaining_currency,
                                                                                  payment_method_name=payment_method)


                            payment_order_response_2['purchase_id'] = purchase_invoice['purchase_invoice']['transaction_no']
                            payment_res = transaction_response['payments']
                            payment_res.append(payment_order_response_2)
                            # pprint.pprint(purchase_order_payment)

                            return transaction_response
                        else:
                            return transaction_response
                    else:
                        print("payments data not available")
                        return transaction_response
        else:
            print('transaction already satisfied')
            return transaction_response
    elif purchase_order.get("remaining", None) and purchase_order.get("remaining", None) == '(Nilai harus lebih besar dari 0.)':
        print('Nilai deposit lebih besar dari nilai transaksi')
        print('deposit: %s' % deposit)
        print('nilai transaksi: %s' % data['total_incl_tax'])
    else:
        while purchase_order.get("person_name", None):
            print('creating purchase order failed: creating contact')
            # pprint.pprint(purchase_order)
            new_contact = create_contact(display_name=person_name.strip(),
                                         email=None,
                                         address=address if address is not None else shipping_address,
                                         billing_address=address if address is not None else shipping_address,
                                         is_customer=False,
                                         is_vendor=True)
            # pprint.pprint(new_contact.json())
            if new_contact.json().get('person', None):
                print(f'{person_name} contact has been created')
            if new_contact.json().get('errors', None) and new_contact.json().get('errors', None) == 'Custom ID sudah pernah dipakai':
                return

            purchase_order = create_purchase_order(transaction_date=transaction_date_po,
                                                   shipping_date=shipping_date,
                                                   shipping_price=shipping_price,
                                                   shipping_address=shipping_address,
                                                   ship_via=ship_via,
                                                   tracking_no=tracking_no,
                                                   address=address,
                                                   term_name=term_name,
                                                   due_date=due_date,
                                                   deposit=deposit,
                                                   discount_unit=discount_unit,
                                                   person_name=person_name,
                                                   warehouse_name=warehouse_name,
                                                   email=email,
                                                   transaction_no=transaction_no,
                                                   memo=memo,
                                                   products=products,
                                                   total_transaction=total_transaction)

        # pprint.pprint(purchase_order)
        if purchase_order.get("error_full_messages", None):
            purchase_order_response['error_message'] = purchase_order['error_full_messages']
            return
        purchase_order_response['message'] = 'created'
        if not data['deliveries']:
            print("deliveries data is not available")
            transaction_response['deliveries'] = ['deliveries and invoices data is not complete']
            transaction_response['invoices'] = ['deliveries and invoices data is not complete']
            transaction_response['payments'] = ['deliveries and invoices data is not complete']
            return transaction_response
        else:
            print("creating delivery and invoice order")
            deliveries = data['deliveries']
            invoices = data['invoices']

            del_and_inv = []
            for x in zip(deliveries, invoices):
                del_and_inv.append(x)

            if len(del_and_inv) == 0:
                print('deliveries and invoices data is not complete')
                transaction_response['deliveries'] = ['deliveries and invoices data is not complete']
                transaction_response['invoices'] = ['deliveries and invoices data is not complete']
                transaction_response['payments'] = ['deliveries and invoices data is not complete']
                return transaction_response

            deliveries_list = transaction_response['deliveries']
            invoices_list = transaction_response['invoices']
            for task in del_and_inv:

                delivery_order_response = dict(
                    order_no=transaction_no,
                    delivery_order_id=None,
                    message=None,
                    error_message=None,
                    jurnal_message=None
                )

                invoice_order_response = dict(
                    order_no=transaction_no,
                    invoice_order_id=None,
                    message=None,
                    error_message=None,
                    jurnal_message=None
                )

                print("creating %s" % task[0]['id'])
                shipping_date = "%s/%s/%s" % (task[0]['delivery_date'][8:10],
                                              task[0]['delivery_date'][5:7],
                                              task[0]['delivery_date'][:4])
                shipping_address = task[0]['ship_to'] if task[0]['ship_to'] is not None else daruma_address
                delivery_no = task[0]['delivery_no']
                selected_po_id = get_purchase_order_id_by_transaction_no(transaction_no=transaction_no)
                purchase_delivery = create_purchase_delivery(person_name=person_name,
                                                             shipping_address=shipping_address,
                                                             transaction_date=shipping_date,
                                                             ship_via=ship_via,
                                                             tracking_no=tracking_no,
                                                             transaction_no=transaction_no,
                                                             reference_no=None,
                                                             selected_po_id=selected_po_id,
                                                             products=products,
                                                             shipping_price=shipping_price,
                                                             memo=memo,
                                                             message=None,
                                                             custom_id=delivery_no)
                # pprint.pprint(purchase_delivery)

                while purchase_delivery.get("error_full_messages", None):

                    selected_po_id = get_purchase_order_id_by_transaction_no(transaction_no=transaction_no)

                    purchase_delivery = create_purchase_delivery(person_name=person_name,
                                                                 shipping_address=shipping_address,
                                                                 transaction_date=shipping_date,
                                                                 ship_via=ship_via,
                                                                 tracking_no=tracking_no,
                                                                 transaction_no=transaction_no,
                                                                 reference_no=None,
                                                                 selected_po_id=selected_po_id,
                                                                 products=products,
                                                                 shipping_price=shipping_price,
                                                                 memo=memo,
                                                                 message=None,
                                                                 custom_id=delivery_no)
                # sync with purchase delivery

                    # pprint.pprint(purchase_delivery)
                    if purchase_delivery.get('product di transaction_lines_attributes untuk baris ke-1', None):
                        delivery_order_response['jurnal_message'] = purchase_delivery.get('product di transaction_lines_attributes untuk baris ke-1', None)
                        deliveries_list.append(delivery_order_response)
                        return

                delivery_order_response['message'] = 'created'

                purchase_delivery_id = purchase_delivery['purchase_delivery']['id']

                delivery_order_response['delivery_order_id'] = purchase_delivery_id

                deliveries_list.append(delivery_order_response)

                invoice_date = "%s/%s/%s" % (task[1]['invoice_date'][8:10],
                                             task[1]['invoice_date'][5:7],
                                             task[1]['invoice_date'][:4])

                purchase_invoice = convert_to_purchase_invoice(id=purchase_delivery_id, invoice_date=invoice_date)

                # pprint.pprint(purchase_invoice)

                invoice_order_response['message'] = 'created'
                invoice_order_response['invoice_order_id'] = purchase_invoice['purchase_invoice']['id']
                invoices_list.append(invoice_order_response)

                if str(term_name) != 'CBD':
                    print("this is not CBD")
                    payments = data['payments']
                    if payments:
                        # pprint.pprint(payments)

                        purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
                                                                              transaction_no=transaction_no,
                                                                              transaction_date=paid_date,
                                                                              purchase_id=purchase_invoice['purchase_invoice'][
                                                                                  'transaction_no'],
                                                                              amount_receive_payment=paid_amount,
                                                                              payment_method_name=payment_method)

                        # pprint.pprint(purchase_order_payment)

                        print("creating purchase invoice 2")
                        remaining_currency = purchase_invoice['purchase_invoice']['remaining']


                        if float(remaining_currency) > 0.0:
                            purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
                                                                                  transaction_no=transaction_no,
                                                                                  transaction_date=paid_date,
                                                                                  purchase_id=purchase_invoice['purchase_invoice'][
                                                                                      'transaction_no'],
                                                                                  amount_receive_payment=remaining_currency,
                                                                                  payment_method_name=payment_method)

                            # pprint.pprint(purchase_order_payment)
                            return
                        else:
                            return
                    else:
                        print("payments data not available")
                        return transaction_response
            return transaction_response



def purchase_insert_bulk(filename, index=0, until=0):
    sample_data = read_sample_input(filename)[index:until]
    report = []
    with open('index.txt', 'r') as i:
        index_log = i.read()
        index_log = int(index_log) if len(index_log) is not 0 else 0
        i.close()

    current = index if index is not 0 else index_log
    for data in sample_data:
        print("\nindex: %s" % current)
        try:
            pur = purchase(data=data)
            report.append(pur)
        except Exception as e:
            tb = traceback.format_exc()
            print(f'purchase failed at index {current}')
            print(f'error message: {e.__repr__()}')
            print(f'traceback: {tb}')
            with open('index.txt', 'w') as i:
                i.write(str(current))
                i.close()

            purchase_insert_bulk(filename=filename, index=current)

        current += 1
    print('insert purchase bulk finished.')
    with open('index.txt', 'w') as i:
        i.write('')
        i.close()
    return report

if __name__ == "__main__":
    # sample_data = read_sample_input("purchase_order_2.json")[1]
    sample_data = read_sample_input("purchase_order_2.json")
    sample_data_with_payments = [i for i in sample_data if i['payments'] and i['invoices']]
    purch = purchase(data=sample_data[1])
    # pprint.pprint(purch)
    # # pprint.pprint(sample_data['payment_term'])
    # # pprint.pprint(sample_data)
    # with_payments = [s for s in sample_data if len(s['payments']) != 0]
    # # pprint.pprint(with_payments[0])

    # bulk = purchase_insert_bulk(filename="purchase_order_2.json", until=15, index=9)
    # keys = bulk[0].keys()
    # pprint.pprint(bulk)
    # pprint.pprint(bulk[0])
    # with open('order_response.csv', 'w') as output_file:
    #     dict_writer = csv.DictWriter(output_file, keys)
    #     dict_writer.writeheader()
    #     dict_writer.writerows(bulk)

    # new_contact = create_contact(display_name='Samson',
    #                              email='Samson@daruma.co.id',
    #                              address='Jakarta',
    #                              is_vendor=True)
    # print('body')
    # # pprint.pprint(json.loads(new_contact.request.body))
    # print('response')
    # # pprint.pprint(new_contact.json())

    # print(checking_purchase_orders('PO-000241'))
    # purchase(data=sample_data[8])

    # current = 184
    # for data in sample_data:
    #     print("\nindex: %s" % current)
    #     current += 1
    #     purchase(data=data)

    # purchase(sample_data[4:])