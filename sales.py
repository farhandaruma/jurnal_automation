from main import *

def sales(data):

    daruma_address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
    purchase_invoice = None
    due_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                             data['invoices'][0]['invoice_date'][5:7],
                             data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else "%s/%s/%s" % (
    datetime.datetime.now().day, datetime.datetime.now().month, datetime.datetime.now().year)
    shipping_date = "%s/%s/%s" % (data['deliveries'][0]['delivery_date'][8:10],
                                  data['deliveries'][0]['delivery_date'][5:7],
                                  data['deliveries'][0]['delivery_date'][:4]) if len(data['deliveries']) > 0 else None
    shipping_price = data['invoices'][0]['shipping_fee'] if len(data['invoices']) > 0 else None
    shipping_address = data['deliveries'][0]['ship_to'] if len(data['deliveries']) > 0 and data['deliveries'][0][
        'ship_to'] is not None else daruma_address
    paid_date = "%s/%s/%s" % (data['payments'][0]['paid_at'][8:10],
                              data['payments'][0]['paid_at'][5:7],
                              data['payments'][0]['paid_at'][:4]) if len(data['payments']) > 0 else None
    ship_via = data['shipping_method'] if data['shipping_method'] is not 'None' else None
    tracking_no = None
    address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
    term_name = data['payment_term']
    transaction_date_po = "%s/%s/%s" % (data['order_date'][8:10],
                                        data['order_date'][5:7],
                                        data['order_date'][:4])
    deposit = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else 0
    discount_unit = None
    person_name = "%s %s".strip() % (data['contact']['first_name'], data['contact']['last_name'])
    warehouse_name = "Gudang Garam"
    email = None
    paid_amount = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else None
    transaction_no = data['order_no']
    memo = None
    products = data['lines']
    payment_method = data['payments'][0]['payment_method'] if len(data['payments']) > 0 else "Cash"
    invoice_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                                 data['invoices'][0]['invoice_date'][5:7],
                                 data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else None

    print("creating purchase for transaction no: %s" % transaction_no)
    total_transaction = data['total_incl_tax']


    sales_order = create_sales_order(transaction_date=transaction_date_so,
                                     shipping_date=shipping_date,
                                     reference_no=reference_no,
                                     tracking_no=tracking_no,
                                     address=address,
                                     due_date=due_date,
                                     person_name=person_name,
                                     warehouse_name=warehouse_name,
                                     deposit=deposit,
                                     discount_unit=discount_unit,
                                     transaction_no=transaction_no,
                                     ship_via=ship_via,
                                     term_name=term_name,
                                     shipping_address=shipping_address,
                                     shipping_price=shipping_price,
                                     email=email,
                                     custom_id=transaction_no,
                                     memo=memo,
                                     message=message,
                                     products=products)

    while sales_order.get("person_name", None) is not None:
        create_contact(display_name=person_name,
                       email=None,
                       address=address if address is not None else shipping_address,
                       billing_address=address if address is not None else shipping_address,
                       custom_id=str(person_name).strip(),
                       is_customer=True,
                       is_vendor=False)
        sales_order = create_sales_order(transaction_date=transaction_date_so,
                                         shipping_date=shipping_date,
                                         reference_no=reference_no,
                                         tracking_no=tracking_no,
                                         address=address,
                                         due_date=due_date,
                                         person_name=person_name,
                                         warehouse_name=warehouse_name,
                                         deposit=deposit,
                                         discount_unit=discount_unit,
                                         transaction_no=transaction_no,
                                         ship_via=ship_via,
                                         term_name=term_name,
                                         shipping_address=shipping_address,
                                         shipping_price=shipping_price,
                                         email=email,
                                         custom_id=transaction_no,
                                         memo=memo,
                                         message=message,
                                         products=products)

    try:
        print("sales order created at: %s" % sales_order['sales_order']['created_at'])
        # # pprint.pprint(sales_order)
    except:
        # pprint.pprint(sales_order)
        return

    selected_po_id = get_sales_order_id_by_transaction_no(transaction_no=transaction_no)

    sales_delivery = create_sales_delivery(person_name=person_name,
                                           shipping_address=shipping_address,
                                           transaction_date=transaction_delivery_date_so,
                                           ship_via=ship_via,
                                           tracking_no=tracking_no,
                                           selected_po_id=selected_po_id,
                                           shipping_price=shipping_price,
                                           memo=memo,
                                           message=message,
                                           reference_no=reference_no,
                                           tax_after_discount=True,
                                           email=email,
                                           transaction_no=transaction_no,
                                           products=products)

    while sales_delivery.get("error_full_messages", None) is not None:
        selected_po_id = get_sales_order_id_by_transaction_no(transaction_no=transaction_no)
        sales_delivery = create_sales_delivery(person_name=person_name,
                                               shipping_address=shipping_address,
                                               transaction_date=transaction_delivery_date_so,
                                               ship_via=ship_via,
                                               tracking_no=tracking_no,
                                               selected_po_id=selected_po_id,
                                               shipping_price=shipping_price,
                                               memo=memo,
                                               message=message,
                                               reference_no=reference_no,
                                               email=email,
                                               transaction_no=transaction_no,
                                               products=products)

    # pprint.pprint(sales_delivery)

    sales_delivery_id = sales_delivery['sales_delivery']['id']

    sales_invoice = convert_to_sales_invoice(sales_delivery_id, invoice_date)

    try:
        print("sales invoice created at: %s" % sales_invoice['sales_invoice']['created_at'])
        # pprint.pprint(sales_invoice)
    except:
        # pprint.pprint(sales_invoice)
        return

    if str(term_name) != "CBD":
        sales_order_payment = add_receive_payment_sales(amount=paid_amount,
                                                        custom_id=transaction_no,
                                                        transaction_date=paid_date,
                                                        sales_id=sales_invoice['sales_invoice']['transaction_no'],
                                                        payment_method_name=payment_method)

        # pprint.pprint(sales_order_payment)



if __name__ == "__main__":
    sample_data = read_sample_input("sample_data.txt")
    data_sales = sample_data[4:8]
    sales(data_sales)
    # # pprint.pprint(data_sales)
    # # pprint.pprint(create_sales_order_payment(person_name="Melinda",
    #                                          transaction_date="14-01-2019",
    #                                          transaction_no="SO-000553"))
