from main import *
import pprint
import time
import traceback

def sales(data):

    daruma_address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
    purchase_invoice = None
    due_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                             data['invoices'][0]['invoice_date'][5:7],
                             data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else "%s/%s/%s" % (
        datetime.datetime.now().day, datetime.datetime.now().month, datetime.datetime.now().year)
    shipping_date = "%s/%s/%s" % (data['deliveries'][0]['delivery_date'][8:10],
                                  data['deliveries'][0]['delivery_date'][5:7],
                                  data['deliveries'][0]['delivery_date'][:4]) if len(data['deliveries']) > 0 else None
    shipping_price = data['invoices'][0]['shipping_fee'] if len(data['invoices']) > 0 else None
    shipping_address = data['deliveries'][0]['ship_to'] if len(data['deliveries']) > 0 and data['deliveries'][0][
        'ship_to'] is not None else daruma_address

    paid_date = None
    try:
        paid_date = "%s/%s/%s" % (data['payments'][0]['paid_at'][8:10],
                                  data['payments'][0]['paid_at'][5:7],
                                  data['payments'][0]['paid_at'][:4]) if data['payments'] else None
    except:
        paid_date = None


    ship_via = data['shipping_method'] if data['shipping_method'] is not 'None' else None
    tracking_no = None
    address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
    term_name = data['payment_term']

    deposit = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else 0
    discount_unit = None
    person_name = "%s %s".strip() % (data['user']['first_name'], data['user']['last_name'])
    warehouse_name = "Gudang Garam"
    email = None
    paid_amount = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else None
    transaction_no = data['order_no']
    memo = None
    products = data['lines']

    payment_method = data['payments'][0].get('payment_method', None) if len(data['payments']) > 0 and data['payments'][0].get('payment_method', None) else "Cash"
    invoice_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                                 data['invoices'][0]['invoice_date'][5:7],
                                 data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else None

    print("creating purchase for transaction no: %s" % transaction_no)
    total_transaction = data['total_incl_tax']

    sales_order_response = dict(
        order_no=transaction_no,
        sales_order_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    sales_delivery_response = dict(
        order_no=transaction_no,
        sales_delivery_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    sales_invoice_response = dict(
        order_no=transaction_no,
        sales_invoice_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    sales_payment_response = dict(
        order_no=transaction_no,
        sales_payment_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    transaction_response = dict(
        # order_no=transaction_no,
        sales_order=sales_order_response,
        delivery_order=sales_delivery_response,
        invoice_order=sales_invoice_response,
        payment_order=sales_payment_response,
        warning=[]
    )

    if data['order_date'] == None:
        sales_order_response['jurnal_message'] = ''
        sales_order_response['message'] = 'order date not available'
        transaction_response['delivery_order'] = 'not created'
        transaction_response['invoice_order'] = 'not created'
        transaction_response['payment_order'] = 'not created'
        return transaction_response

    transaction_date_so = "%s/%s/%s" % (data['order_date'][8:10],
                                        data['order_date'][5:7],
                                        data['order_date'][:4])



    checking_so = get_sales_order(transaction_no=transaction_no).json()

    if checking_so.get('sales_order', None):
        sales_order_response['message'] = 'already exists'
        sales_order_response['sales_order_id'] = checking_so['sales_order']['transaction_no']
    else:
        # create new sales order
        sales_order = create_sales_order(transaction_date=transaction_date_so,
                                         shipping_date=shipping_date,
                                         reference_no=None,
                                         tracking_no=tracking_no,
                                         address=shipping_address,
                                         due_date=due_date,
                                         person_name=person_name,
                                         warehouse_name=warehouse_name,
                                         deposit=deposit,
                                         discount_unit=discount_unit,
                                         transaction_no=transaction_no,
                                         ship_via=ship_via,
                                         products=products,
                                         shipping_price=shipping_price,
                                         total_transaction=total_transaction,
                                         is_shipped=True,
                                         term_name=term_name,
                                         shipping_address=shipping_address,
                                         email=email,
                                         custom_id=transaction_no)
        if sales_order.get('error_full_messages', None) is not None:
            sales_order_response['jurnal_message'] = sales_order.get('error_full_messages')

            if sales_order.get('error_full_messages', None) == []:
                sales_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response

            if sales_order.get('remaining', None):
                print('total transaction: %s' % total_transaction)
                print('deposit: %s' % deposit)
                print('term name: %s' % term_name)
                sales_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response

        if sales_order.get("person_name", None):
            # print('creating purchase order failed: creating contact')
            # pprint.pprint(purchase_order)
            new_contact = create_contact(display_name=f'{person_name.strip()}',
                                         email=None,
                                         address=None,
                                         billing_address=address if address is not None else None,
                                         is_customer=True,
                                         is_vendor=False,
                                         people_type='customer')

            if new_contact.json().get('person', None):

                print(f'{person_name} contact has been created')
            if new_contact.json().get('errors', None) == 'Custom ID sudah pernah dipakai':
                sales_order_response['jurnal_message'] = sales_order.get('error_full_messages')
                sales_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response
            sales_order = create_sales_order(transaction_date=transaction_date_so,
                                             shipping_date=shipping_date,
                                             reference_no=None,
                                             tracking_no=tracking_no,
                                             address=shipping_address,
                                             due_date=due_date,
                                             person_name=person_name,
                                             warehouse_name=warehouse_name,
                                             deposit=deposit,
                                             discount_unit=discount_unit,
                                             transaction_no=transaction_no,
                                             ship_via=ship_via,
                                             products=products,
                                             shipping_price=shipping_price,
                                             total_transaction=total_transaction,
                                             is_shipped=True,
                                             term_name=term_name,
                                             shipping_address=shipping_address,
                                             email=email,
                                             custom_id=transaction_no)
            if sales_order.get('error_full_messages', None):
                sales_order_response['jurnal_message'] = sales_order.get('error_full_messages')
                sales_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response
            sales_order_response['message'] = 'created'
            sales_order_response['sales_order_id'] = sales_order['sales_order']['transaction_no']


        if sales_order.get('error_full_messages', None):
            sales_order_response['jurnal_message'] = sales_order.get('error_full_messages')
            sales_order_response['message'] = 'failed'
            transaction_response['delivery_order'] = 'not created'
            transaction_response['invoice_order'] = 'not created'
            transaction_response['payment_order'] = 'not created'
            return transaction_response
        else:
            sales_order_response['message'] = 'created'
            # pprint.pprint(purchase_order)
            if sales_order.get('error_full_messages', None) is not None:
                sales_order_response['jurnal_message'] = sales_order.get('error_full_messages', None)
            elif sales_order.get('errors', None) is not None:
                sales_order_response['jurnal_message'] = sales_order.get('errors', None)
            else:
                pprint.pprint(sales_order)
                sales_order_response['sales_order_id'] = sales_order['sales_order']['transaction_no']

    # create sales delivery
    deliveries = data.get('deliveries', None)
    if deliveries:

        delivery = deliveries[0]
        checking_delivery = get_sales_delivery(delivery['delivery_no']).json()

        # if 'purchase_delivery' key is not available in json response, so create new purchase delivery
        if not checking_delivery.get('sales_delivery', None):
            delivery_no = delivery['delivery_no']
            selected_po_id = get_sales_order_id_by_transaction_no(transaction_no=transaction_no)
            if not selected_po_id:
                print('selected po id is None')
                sales_delivery_response['message'] = 'failed'
                transaction_response['deliveries'] = 'selected po id is not found'
                transaction_response['invoices'] = 'not created'
                transaction_response['payments'] = 'not created'
                return transaction_response
            sales_delivery = create_sales_delivery(person_name=person_name,
                                                         shipping_address=shipping_address,
                                                         transaction_date=shipping_date,
                                                         ship_via=ship_via,
                                                         tracking_no=tracking_no,
                                                         transaction_no=transaction_no,
                                                         reference_no=None,
                                                         selected_po_id=selected_po_id,
                                                         products=products,
                                                         shipping_price=shipping_price,
                                                         memo=memo,
                                                         message=None,
                                                         custom_id=delivery_no)
            sales_delivery_response['sales_delivery_id'] = sales_delivery['purchase_delivery']['custom_id']
            sales_delivery_response['message'] = 'created'
        else:
            # print('[deliveries already created]')
            sales_delivery_response['message'] = 'already exists'
            sales_delivery_response['sales_delivery_id'] = checking_delivery['sales_delivery']['custom_id']
    else:
        # print('deliveries data not available')
        sales_delivery_response['message'] = 'deliveries data not available'

    invoices = data.get('invoices', None)
    payments = data.get('payments', None)
    deliveries = data.get('deliveries', None)
    delivery = deliveries[0] if deliveries else None
    if invoices and delivery:
        delivery_no = delivery['delivery_no']
        sales_del = get_sales_delivery(delivery_no).json().get('sales_delivery', None)
        get_sales_invoice_id = sales_del if sales_del is not None else None
        get_sales_invoice_id = get_sales_invoice_id['sales_invoice'][ 'id'] if get_sales_invoice_id is not None and get_sales_invoice_id.get('sales_invoice', None) else None
        if not sales_del:
            # pprint.pprint(get_purchase_invoice(get_purchase_invoice_id).json())
            sales_delivery_response['message'] = 'failed'
            sales_delivery_response['jurnal_message'] = get_sales_invoice(get_sales_invoice_id).json()
            return transaction_response
        if not get_sales_invoice_id:
            # create purchase invoice and payment if payments data exists
            sales_delivery_id = get_sales_delivery(delivery['delivery_no']).json()['sales_delivery']['id']
            sales_invoice = convert_to_sales_invoice(id=sales_delivery_id, invoice_date=invoice_date)
            sales_invoice_response['sales_invoice_id'] = sales_invoice.json()['sales_invoice']['transaction_no']
            sales_invoice_response['message']='created'

            if str(term_name) != 'CBD':
                print("this is not CBD 1")
                if payments:
                    sales_order_payment = add_receive_payment_sales(transaction_date=paid_date,
                                                                    sales_id=sales_invoice.json()['sales_invoice']['transaction_no'],
                                                                    amount=paid_amount,
                                                                    payment_method_name=payment_method,
                                                                    custom_id=transaction_no,
                                                                    memo=None)
                    if sales_order_payment.get('error_full_messages', None) is not None:
                        sales_order_payment['jurnal_message'] = sales_order_payment.get('error_full_messages')
                        sales_order_payment['message'] = 'failed'
                        return transaction_response
                    else:
                        sales_payment_response['message'] = 'created'
                        sales_payment_response['sales_payment_id'] = sales_payment_response['sales_payment']['custom_id']
        else:
            # print('[invoices already exists]')
            sales_invoice_response['message'] = 'already exists'
            sales_invoice_response['sales_invoice_id'] = get_sales_invoice_id
    else:
        # print('invoices data is not available')
        sales_invoice_response['message'] = 'invoices data not available'

    payments = data.get('payments', None)
    if payments and delivery:
        delivery_no = delivery['delivery_no']
        get_sales_invoice_id = get_sales_delivery(delivery_no).json().get('sales_delivery', None)['sales_invoice']['id']
        if get_sales_invoice_id and str(term_name) != 'CBD':
            delivery_no = delivery['delivery_no']
            get_sales_invoice_id = get_sales_delivery(delivery_no).json()['sales_delivery']['sales_invoice']['id']
            checking_invoice = get_sales_invoice(get_sales_invoice_id).json()
            if checking_invoice.get("sales_invoice", None) and str(checking_invoice['sales_invoice']['transaction_status']['name']).strip().lower() != 'paid':
                # create payment
                print('this is not CBD 3')
                sales_order_payment = add_receive_payment_sales(transaction_date=paid_date,
                                                                custom_id=transaction_no,
                                                                sales_id=checking_invoice['purchase_invoice']['transaction_no'],
                                                                amount=paid_amount,
                                                                payment_method_name=payment_method)

                if sales_order_payment.get('error_full_messages', None):
                    sales_payment_response['message'] = 'failed'
                    sales_payment_response['jurnal_message'] = sales_order_payment['error_full_messages']
                    print('payment failed')
                    # pprint.pprint(purchase_order_payment)
                    print('paid ammount: %s' % paid_amount)
                    return transaction_response
                else:
                    sales_payment_response['sales_payment_id'] = sales_order_payment['sales_payment']['custom_id']
                    sales_payment_response['message'] = 'created'
            else:
                sales_payment_response['sales_payment_id'] = get_sales_invoice_id
                sales_payment_response['message'] = 'already exists'
        else:
            print('this is CBD, payments data not processed')
            sales_payment_response['message'] = 'this is CBD, payments data not processed'
            if get_sales_invoice_id:
                checking_invoice = get_sales_invoice(get_sales_invoice_id).json()
                status_invoice = checking_invoice['sales_invoice']['transaction_status']['name']
                print('status invoice: %s' % status_invoice)
                remaining = get_sales_invoice(get_sales_invoice_id).json()['sales_invoice']['remaining']
                if float(remaining) > 0.0 and str(status_invoice).lower().strip() == 'overdue':
                    sales_order_payment_2 = add_receive_payment_sales(
                        custom_id=transaction_no,
                        transaction_date=due_date,
                        sales_id=
                        checking_invoice['sales_invoice'][
                            'transaction_no'],
                        amount=remaining,
                        payment_method_name=payment_method)
                    if sales_order_payment_2.get('error_full_messages', None):
                        sales_payment_response['message'] = 'sales_order_payment_2 failed'
                        sales_payment_response['jurnal_message'] = sales_order_payment_2['error_full_messages']
                        return transaction_response

    else:
        # print('payments data not available')
        sales_payment_response['message'] = 'payment data not available'
    return transaction_response

def sales_insert_bulk(filename, index=0, until=0):

    with open('sales_response.json', 'w') as output_file:
        json.dump([], output_file)

    json_data = open('sales_response.json', 'r')
    json_data = json.load(json_data)

    def go_bulk(data):
        try:
            sal = sales(data=data)
            pprint.pprint(sal)
            return sal
        except Exception as e:
            tb = traceback.format_exc()
            print(f'purchase failed at index {current}')
            print(f'error message: {e.__repr__()}')
            print(f'traceback: {tb}')
            with open('index_sales.txt', 'w') as i:
                i.write(str(current))
                i.close()
            time.sleep(5)
            with open('index_sales.txt', 'w') as i:
                i.write('')
                i.close()
            return None

    report = []

    with open('index_sales.txt', 'r') as i:
        index_log = i.read()
        index_log = int(index_log) if len(index_log) is not 0 else 0
        i.close()

    current = index if index is not 0 else index_log

    if until == 0:
        sample_data = read_sample_input(filename)[current:]
    else:
        sample_data = read_sample_input(filename)[current:until]
    for data in sample_data:
        print('------------------transaction no: %s index: %s------------------' % (data['order_no'], current))
        run = go_bulk(data)
        while run == None:
            run = go_bulk(data)
        s = run
        current += 1
        if s:
            with open('sales_response.json', 'w') as f:
                # writer = csv.writer(f)
                # writer.writerow()
                json_data.append(s)
                json.dump(json_data, f)

    print('insert sales bulk finished.')
    with open('index_sales.txt', 'w') as i:
        i.write('')
        i.close()
    return report

all_data = read_sample_input('sales_order_2.json')
completed_data = [i for i in all_data if i['deliveries'] and i['invoices'] and i['payments']]
sample = completed_data[0]
bulk = sales_insert_bulk(filename='sales_order_2.json')
print(bulk)

# pprint.pprint(all_data[3])




# data = all_data[1]
#
# daruma_address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
# shipping_address = data['deliveries'][0]['ship_to'] if len(data['deliveries']) > 0 and data['deliveries'][0][
#         'ship_to'] is not None else daruma_address
# transaction_date_so = "%s/%s/%s" % (data['order_date'][8:10],
#                                         data['order_date'][5:7],
#                                         data['order_date'][:4])
# shipping_date = "%s/%s/%s" % (data['deliveries'][0]['delivery_date'][8:10],
#                                   data['deliveries'][0]['delivery_date'][5:7],
#                                   data['deliveries'][0]['delivery_date'][:4]) if len(data['deliveries']) > 0 else None
#
# due_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
#                              data['invoices'][0]['invoice_date'][5:7],
#                              data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else "%s/%s/%s" % (
#         datetime.datetime.now().day, datetime.datetime.now().month, datetime.datetime.now().year)
#
# person_name = "%s %s".strip() % (data['user']['first_name'], data['user']['last_name'])
# warehouse_name = "Gudang Garam"
# deposit = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else 0
# discount_unit = None
# transaction_no = data['order_no']
# products = data['lines']
# ship_via = data['shipping_method'] if data['shipping_method'] is not 'None' else None
# shipping_price = data['invoices'][0]['shipping_fee'] if len(data['invoices']) > 0 else None
# total_transaction = data['total_incl_tax']
# term_name = data['payment_term']
# # pprint.pprint(all_data[1])
# sales_test = create_sales_order(transaction_date=transaction_date_so,
#                            shipping_date=shipping_date,
#                                 reference_no=None,
#                                 tracking_no=None,
#                                 address=shipping_address,
#                                 due_date=due_date,
#                                 person_name=person_name,
#                                 warehouse_name=warehouse_name,
#                                 deposit=deposit,
#                                 discount_unit=None,
#                                 transaction_no=transaction_no,
#                                 ship_via=ship_via,
#                                 products=products,
#                                 shipping_price=shipping_price,
#                                 total_transaction=total_transaction,
#                                 term_name=term_name,
#                                 shipping_address=shipping_address)
#
# pprint.pprint(sales_test)