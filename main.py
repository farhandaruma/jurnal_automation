import _constants
import requests
import json
import datetime
import pprint

headers = {
        'apikey': _constants.API_KEY,
    }

def create_contact(display_name,
                   email,
                   address,
                   people_type,
                   billing_address=None,
                   phone=None,
                   first_name=None,
                   last_name=None,
                   is_customer=None,
                   is_vendor=None,
                   mobile=None,
                   ):


    data_create_contact = {
                              "person": {
                                "id": None,
                                "display_name": display_name,
                                "title": None,
                                "first_name": first_name,
                                "middle_name": None,
                                "last_name": last_name,
                                "mobile": mobile,
                                "identity_type": None,
                                "identity_number": None,
                                "email": email,
                                "other_detail": None,
                                "associate_company": None,
                                "phone": phone,
                                "fax": None,
                                "tax_no": None,
                                "archive": None,
                                "billing_address": billing_address if billing_address is not None else address,
                                "billing_address_no": None,
                                "billing_address_rt": None,
                                "billing_address_rw": None,
                                "billing_address_post_code": None,
                                "billing_address_kelurahan": None,
                                "billing_address_kecamatan": None,
                                "billing_address_kabupaten": None,
                                "billing_address_provinsi": None,
                                "address": address,
                                "bank_account_details": [
                                  {
                                    "bank_name": None,
                                    "bank_branch": "",
                                    "bank_account_holder_name": "",
                                    "bank_account_number": ""
                                  }
                                ],
                                "default_ar_account_id": None,
                                "default_ap_account_id": None,
                                "disable_max_credit_limit": True,
                                "disable_max_debit_limit": True,
                                "max_credit_limit": None,
                                "max_debit_limit": None,
                                "term_id": None,
                                "people_type": people_type,
                                "is_customer": is_customer,
                                "is_vendor": is_vendor
                              }
                        }
    url = 'https://sandbox-api.jurnal.id/core/api/v1/contacts'

    response = requests.post(url=url, json=data_create_contact, headers=headers)



    return response


def add_bank_deposit(deposit_amount,
                     transaction_date,
                     transaction_no,
                     person_name,
                     deposit_to_name):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/bank_deposits'

    data_deposit = {
        "bank_deposit": {
            "deposit_to_name": deposit_to_name,
            "person_name": person_name,
            "transaction_date": transaction_date,
            "transaction_no": transaction_no,
            "memo": "memo goes here",
            "custom_id": transaction_no,
            "transaction_account_lines_attributes": [
                {
                    "account_name": "Kas",
                    "description": "description of account lines",
                    "credit": deposit_amount,
                    # "line_tax_id": 11562,
                    "line_tax_name": "PPN"
                }
            ]
        }
    }

    response = requests.post(url=url, headers=headers, json=data_deposit)
    return response.json()


def create_purchase_order(transaction_date,
                          shipping_date,
                          shipping_price,
                          shipping_address,
                          ship_via,
                          tracking_no,
                          address,
                          term_name,
                          due_date,
                          deposit,
                          total_transaction,
                          discount_unit,
                          person_name,
                          warehouse_name,
                          email,
                          transaction_no,
                          memo,
                          products,
                          custom_id=None,
                          message=None,
                          reference_no=None,
                          is_shipped=True):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_orders'

    # checking = checking_purchase_orders(transaction_no)
    # if checking:
    #     return checking

    transaction_lines_attributes = []

    for product in products:
        product_id = get_product_id_by_name(product['name'])
        if not product_id:
            print("create product %s" % product['name'])
            p = create_product(name=product['name'],
                               product_code=product['catalog_code'],
                               sell_price=product['price'],
                               buy_price=product['price'],
                               unit_name="Pcs",
                               custom_id=product['catalog_code'])
            if p.get('error_full_messages', None) is not None:
                if p.get('name', None):
                    print("product already satisfied (create_purchase_order)")
        transaction_lines_attributes.append(
            dict(
                quantity=product['quantity'],
                rate=product['price'],
                discount=100 - (float(product['price']) / float(product['price_before_discount']) * 100),
                product_name=product['name'],
                line_tax_name="ppn"
            )
        )

    if str(term_name) == 'CBD' and float(deposit) > float(total_transaction):

        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": str(term_name),
                "due_date": due_date,
                "deposit": total_transaction,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "warehouse_name": warehouse_name,
                "witholding_account_name": "Kas",
                "witholding_value": float(deposit) - float(total_transaction),
                "witholding_type": "value",
                "refund_from_name": "Kas",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }

    elif str(term_name) != 'CBD' and float(deposit) > float(total_transaction):

        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": str(term_name),
                "due_date": due_date,
                # "deposit": deposit,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "warehouse_name": warehouse_name,
                "witholding_account_name": "Kas",
                "witholding_value": float(deposit) - float(total_transaction),
                "witholding_type": "value",
                "refund_from_name": "Kas",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    elif str(term_name) == 'CBD':
        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": term_name,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "deposit": deposit,
                "person_name": person_name,
                "due_date": due_date,
                "warehouse_name": warehouse_name,
                # "witholding_account_name": "Kas",
                # "witholding_value": 10,
                # "witholding_type": "percent",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    elif str(term_name) != 'CBD':
        data_purchase_order = {
            "purchase_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": term_name,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "due_date": due_date,
                "warehouse_name": warehouse_name,
                # "witholding_account_name": "Kas",
                # "witholding_value": deposit - total_transaction,
                # "witholding_type": "value",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    response = requests.post(url=url, headers=headers, json=data_purchase_order)
    return response.json()

def create_sales_order(transaction_date,
                       shipping_date,
                       reference_no,
                       tracking_no,
                       address,
                       due_date,
                       person_name,
                       warehouse_name,
                       deposit,
                       discount_unit,
                       transaction_no,
                       ship_via,
                       products,
                       shipping_price,
                       total_transaction,
                       is_shipped=True,
                       term_name=None,
                       shipping_address=None,
                       email=None,
                       custom_id=None,
                       memo=None,
                       message=None):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_orders'


    transaction_lines_attributes = []

    for product in products:
        product_id = get_product_id_by_name(product['name'])
        if not product_id:
            print("create product %s" % product['name'])
            p = create_product(name=product['name'],
                               product_code=product['catalog_code'],
                               sell_price=product['price'],
                               buy_price=product['price'],
                               unit_name="Pcs",
                               custom_id=product['catalog_code'])
            if p.get('error_full_messages', None) is not None:
                if p.get('name', None):
                    print("product already satisfied (create_sales_order)")
        transaction_lines_attributes.append(
            dict(
                quantity=product['quantity'],
                rate=product['price'],
                discount=100 - (float(product['price']) / float(product['price_before_discount']) * 100),
                product_name=product['name'],
                line_tax_name="ppn"
            )
        )

    if str(term_name) == 'CBD' and float(deposit) > float(total_transaction):
        print(" str(term_name) == 'CBD' and float(deposit) > float(total_transaction)")
        data_sales_order = {
            "sales_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": str(term_name),
                "due_date": due_date,
                "deposit": total_transaction,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "warehouse_name": warehouse_name,
                "witholding_account_name": "Kas",
                "witholding_value": float(deposit) - float(total_transaction),
                "witholding_type": "value",
                "refund_from_name": "Kas",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }

    elif str(term_name) != 'CBD' and float(deposit) > float(total_transaction):
        print("str(term_name) != 'CBD' and float(deposit) > float(total_transaction)")
        data_sales_order = {
            "sales_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": str(term_name),
                "due_date": due_date,
                # "deposit": deposit,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "warehouse_name": warehouse_name,
                "witholding_account_name": "Kas",
                "witholding_value": float(deposit) - float(total_transaction),
                "witholding_type": "value",
                "refund_from_name": "Kas",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    elif str(term_name) == 'CBD':
        print("str(term_name) == 'CBD'")
        data_sales_order = {
            "sales_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": term_name,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "deposit": deposit,
                "person_name": person_name,
                "due_date": due_date,
                "warehouse_name": warehouse_name,
                "deposit_to_name": "Kas",
                # "witholding_value": 10,
                # "witholding_type": "percent",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }
    elif str(term_name) != 'CBD':
        print("str(term_name) != 'CBD'")
        data_sales_order = {
            "sales_order": {
                "transaction_date": transaction_date,
                "transaction_lines_attributes": transaction_lines_attributes,
                "shipping_date": shipping_date,
                "shipping_price": shipping_price,
                "shipping_address": shipping_address if shipping_address is not None else address,
                "is_shipped": is_shipped,
                "ship_via": ship_via,
                "reference_no": reference_no,
                "tracking_no": tracking_no,
                "address": address,
                "term_name": term_name,
                "discount_unit": discount_unit,
                "discount_type_name": "Percent",
                "person_name": person_name,
                "due_date": due_date,
                "warehouse_name": warehouse_name,
                # "witholding_account_name": "Kas",
                # "witholding_value": deposit - total_transaction,
                # "witholding_type": "value",
                "email": email,
                "transaction_no": transaction_no,
                "message": message,
                "memo": memo,
                "custom_id": custom_id
            }
        }

    response = requests.post(url=url, headers=headers, json=data_sales_order)
    return response.json()


def create_sales_order_payment(person_name,
                               transaction_date,
                               transaction_no,
                               payment_method_name,
                               memo=None):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_order_payments'

    data_sales_order_payment = {
        "sales_order_payment": {
            "person_name": person_name,
            "transaction_date": transaction_date,
            "transaction_no": transaction_no,
            "payment_method_name": payment_method_name,
            "deposit_to_name": "Cash",
            "memo": memo,
            "is_draft": False,
            "records_attributes": [
                {
                    "transaction_no": transaction_no,
                    "amount":  2112000
                }
            ]
        }
    }

    response = requests.post(url=url, headers=headers, json=data_sales_order_payment)
    return response.json()


def add_receive_payment_sales(amount,
                              custom_id,
                              transaction_date,
                              sales_id,
                              payment_method_name,
                              memo=None):
    url = 'https://sandbox-api.jurnal.id/core/api/v1/receive_payments'

    receive_payment_data = {
        "receive_payment": {
            "transaction_date": transaction_date,
            "records_attributes": [
                {
                    "transaction_no": sales_id,
                    "amount": amount
                }
            ],
            "custom_id": custom_id,
            "payment_method_name": payment_method_name,
            "is_draft": False,
            "deposit_to_name": "Kas",
            "memo": memo,
        }
    }

    response = requests.post(url=url, headers=headers, json=receive_payment_data)
    return response.json()


def add_receive_payment_purchase(person_name,
                                 transaction_no,
                                 transaction_date,
                                 purchase_id,
                                 amount_receive_payment,
                                 refund_from_name="Kas",
                                 payment_method_name="Cash",
                                 memo=None):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_payments'

    receive_payment_data = {
        "purchase_payment": {
            "transaction_date": transaction_date,
            "transaction_no": transaction_no,
            "records_attributes": [
                {
                    "transaction_no": purchase_id,
                    "amount": amount_receive_payment
                }
            ],
            "person_name": person_name,
            "payment_method_name": payment_method_name,
            "memo": memo,
            "refund_from_name": refund_from_name,
            "custom_id": transaction_no,
            "is_draft": False
        }
    }

    response = requests.post(url=url, headers=headers, json=receive_payment_data)
    return response.json()


def create_warehouse(name,
                     code,
                     address,
                     description,
                     custom_id):
    url = 'https://sandbox-api.jurnal.id/core/api/v1/warehouses/'

    warehouse_data = {
        "warehouse": {
            "name": name,
            "code": code,
            "address": address,
            "description": description,
            "custom_id": custom_id
        }
    }

    response = requests.post(url=url, headers=headers, json=warehouse_data)
    return response.json()


def create_purchase_delivery(person_name,
                             shipping_address,
                             transaction_date,
                             ship_via,
                             tracking_no,
                             transaction_no,
                             reference_no,
                             selected_po_id,
                             products,
                             shipping_price,
                             custom_id,
                             memo=None,
                             message=None,
                             tax_after_discount=True,
                             is_shipped=True):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_deliveries/'

    transaction_lines_attributes = []
    for product in products:
        product_id = get_product_id_by_name(product['name'])
        if not product_id:
            print("create product %s" % product['name'])
            p = create_product(name=product['name'],
                               product_code=product['catalog_code'],
                               sell_price=product['price'],
                               buy_price=product['price'],
                               unit_name="Pcs",
                               custom_id=product['catalog_code'])
            if p.get('error_full_messages', None):
                if p.get('name', None):
                    print("product already satisfied")
                    product_id = p.get('id')
        transaction_lines_attributes.append(dict(
            quantity=product['quantity'],
            product_id=product_id if product_id is not None else get_product_id_by_name(product['name']),
            description="",
            id="",
            name=product['name']
        ))
    # # pprint.pprint(transaction_lines_attributes)
    data_purchase_delivery = {
        "purchase_delivery": {
            "tax_after_discount": tax_after_discount,
            "person_name": person_name,
            "email": "",
            "is_shipped": is_shipped,
            "shipping_address":shipping_address,
            "transaction_date": transaction_date,
            "ship_via": ship_via,
            "tracking_no": tracking_no,
            "transaction_no": transaction_no,
            "reference_no": reference_no,
            "selected_po_id": selected_po_id,
            "transaction_lines_attributes": transaction_lines_attributes,
            "shipping_price": shipping_price,
            "message": message,
            "memo": memo,
            "custom_id": custom_id
        }
    }

    response = requests.post(url=url, headers=headers, json=data_purchase_delivery)

    err_response = response.json().get('error_full_messages', None)
    if err_response:
        if 'Product di transaction lines attributes untuk baris' in err_response[0]:
            # pprint.pprint(response.json())
            return None

    return response.json()


# def convert_to_purchase_invoice(id):
#     url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_deliveries/%s/convert_to_invoice' % id
#
#     response = requests.post(url=url, headers=headers)
#
#     return response.json()


def create_product(name,
                   product_code,
                   sell_price,
                   buy_price,
                   unit_name,
                   description=None,
                   custom_id=None,
                   inventory_asset_account_name="Persediaan Barang Jadi"):

  url = 'https://sandbox-api.jurnal.id/core/api/v1/products'

  data_product = {
    "product": {
      "name": name,
      "sell_price_per_unit": sell_price,
      "custom_id": custom_id,
      "track_inventory": "true",
      "description": description,
      "unit_name": unit_name,
      "buy_price_per_unit": buy_price,
      "product_code": product_code,
      "is_bought": True,
      "buy_account_number": "5-50000",
      "buy_account_name": "Cost of Sales",
      "is_sold": True,
      # "sell_account_number": "4-40000",
      # "sell_account_name": "Service Revenue",
      "inventory_asset_account_name": inventory_asset_account_name,
      "taxable_sell": True,
    }
  }

  response = requests.post(url=url, headers=headers, json=data_product)

  return response.json()

def create_sales_delivery(person_name,
                          shipping_address,
                          transaction_date,
                          ship_via,
                          tracking_no,
                          transaction_no,
                          selected_po_id,
                          shipping_price,
                          products,
                          custom_id,
                          memo=None,
                          message=None,
                          reference_no=None,
                          tax_after_discount=True,
                          email=None,
                          is_shipped=True):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_deliveries/'


    transaction_lines_attributes = []
    for product in products:
        transaction_lines_attributes.append(dict(
            quantity=product['quantity'],
            product_id=get_product_id_by_name(product['name']),
            description="",
            id=""
        ))

    data_sales_delivery = {
        "sales_delivery": {
            "tax_after_discount": tax_after_discount,
            "transaction_type_id": 21,
            "person_name": person_name,
            "email": email,
            "is_shipped": is_shipped,
            "shipping_address": shipping_address,
            "transaction_date": transaction_date,
            "ship_via": ship_via,
            "tracking_no": tracking_no,
            "transaction_no": transaction_no,
            "reference_no": reference_no,
            "tag_ids": None,
            "selected_po_id": selected_po_id,
            "transaction_lines_attributes": transaction_lines_attributes,
            "shipping_price": shipping_price,
            "message": message,
            "memo": memo,
            "custom_id":custom_id
        }
    }

    response = requests.post(url=url, headers=headers, json=data_sales_delivery)

    return response.json()


def convert_to_sales_invoice(id, invoice_date):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_deliveries/%s/convert_to_invoice' % id

    body = {
        "purchase_delivery": {
            "transaction_date": invoice_date,
        }
    }

    response = requests.post(url=url, headers=headers, json=body)

    return response.json()


def convert_to_purchase_invoice(id, invoice_date):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_deliveries/%s/convert_to_invoice' % id

    body = {
        "purchase_delivery": {
            "transaction_date": invoice_date,
        }
    }

    response = requests.post(url=url, headers=headers, json=body)

    return response


def get_list_of_products(page=1):
    url = 'https://sandbox-api.jurnal.id/core/api/v1/products'


    current_page = {'page': page}
    products = requests.get(url=url, headers=headers, params=current_page).json()
    if products.get('current_page', None) is None:
        pprint.pprint(products)
        return
    current_product_page = int(products['current_page'])
    total_product_page = int(products['total_pages'])
    result = []
    while current_product_page <= total_product_page:
        products = requests.get(url=url, headers=headers, params=current_page).json()
        current_product_page = int(products['current_page'])
        total_product_page = int(products['total_pages'])
        for product in products['products']:
            result.append(dict(
                product_name=product['name'],
                product_id=product['id']
            ))
        current_page['page'] += 1

    return result

def get_list_of_terms():
    url = 'https://sandbox-api.jurnal.id/core/api/v1/terms'

    response = requests.get(url=url, headers=headers)

    return response.json()

def get_list_of_sales_order_payment(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_orders/%s/sales_order_payments' % transaction_no

    response = requests.get(url=url, headers=headers)

    return response.json()

def get_list_of_purchase_orders(page=1):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_orders/'

    data = {
        'page': page
    }

    response = requests.get(url=url, headers=headers, params=data)

    return response


def get_purchase_order(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_orders/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_sales_order(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_orders/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_purchase_delivery(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_deliveries/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_sales_delivery(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_deliveries/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_purchase_invoice(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_invoices/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response

def get_sales_invoice(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_invoices/%s' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response


def get_transaction_by_order_no(transaction_no, data):
    for d in data:
        if str(d['order_no']) == transaction_no:
            return d
    return "data not found"

def get_list_of_payments_from_purchase(transaction_no):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_invoices/%s/purchase_payments' % transaction_no


    response = requests.get(url=url, headers=headers)

    return response


def get_list_of_purchase_deliveries(page=1):

    url = 'https://sandbox-api.jurnal.id/core/api/v1/purchase_deliveries'

    data = {
        'page': page
    }

    response = requests.get(url=url, headers=headers, params=data)

    return response



def get_product_id_by_name(name):
    products = get_list_of_products()

    if products:
        for product in products:
            if str(product['product_name']).lower() == name.lower():
                return product['product_id']

    return None

def read_sample_input(filename):
  with open(filename) as file:
    return json.loads(file.read())


def get_list_of_sales_order():

    url = 'https://sandbox-api.jurnal.id/core/api/v1/sales_orders/'

    response = requests.get(url=url, headers=headers)

    return response


def get_sales_order_id_by_transaction_no(transaction_no):
    sales_orders = get_list_of_sales_order().json()['sales_orders']

    for sales in sales_orders:
        if sales['transaction_no'] == transaction_no:
            return sales['id']

    return None

def get_purchase_order_id_by_transaction_no(transaction_no):

    page = 1
    purchase_orders = get_list_of_purchase_orders(page=page).json()
    current_purchase_orders_page = int(purchase_orders['current_page'])
    total_purchase_orders_page = int(purchase_orders['total_pages'])
    while current_purchase_orders_page <= total_purchase_orders_page:
        purchase_orders = get_list_of_purchase_orders(page=page).json()
        current_purchase_orders_page = int(purchase_orders['current_page'])
        total_purchase_orders_page = int(purchase_orders['total_pages'])
        for p in purchase_orders['purchase_orders']:
            if str(p.get('transaction_no')).strip() == str(transaction_no).strip():
                return p.get('id')
        page += 1

    return None


def checking_purchase_orders(transaction_no):

    page = 1
    purchase_orders = get_list_of_purchase_orders(page=page).json()
    current_purchase_orders_page = int(purchase_orders['current_page'])
    total_purchase_orders_page = int(purchase_orders['total_pages'])
    while current_purchase_orders_page <= total_purchase_orders_page:
        purchase_orders = get_list_of_purchase_orders(page=page).json()
        current_purchase_orders_page = int(purchase_orders['current_page'])
        total_purchase_orders_page = int(purchase_orders['total_pages'])
        for p in purchase_orders['purchase_orders']:
            if str(p.get('transaction_no')).strip() == str(transaction_no).strip():
                return p
        page += 1
    return None

def get_list_of_contacts():
    url = 'https://sandbox-api.jurnal.id/core/api/v1/contacts'

    response = requests.get(url=url, headers=headers)

    return response.json()

if __name__ == "__main__":
    # # pprint.pprint(get_list_of_sales_order_payment(1193728))
    # # pprint.pprint(get_list_of_purchase_order().json())
    # sample_data = read_sample_input("sales_order_2.json")
    # # pprint.pprint(sample_data[3])
    # today = datetime.datetime.now()
    # purchase_orders = get_list_of_purchase_orders().json().get('purchase_orders')
    # for po in purchase_orders:
    #     if po.get('transaction_no') == 'PO-000309':
    #         # pprint.pprint(po)
    #         break
    #     print('transaction not available')
    # product = {'brand_name': 'Swipeall',
    #            'catalog_code': 'S060-0002',
    #            'name': 'Swipe-All P80 Oil Sorbent Pad White (Penyerap Oli), 79980',
    #            'price': '540000.00',
    #            'price_before_discount': '675000.00',
    #            'primary_image_url': 'https://media.daruma.co.id/images/products/t/18/swipe-all-p80-pad-40-x-50-cm-image.jpg',
    #            'quantity': 1,
    #            'tax_code': 'PPN',
    #            'uid': '1893426523'}

    # # pprint.pprint(get_list_of_purchase_orders().json()['purchase_orders'])


    # new_warehouse = create_warehouse(name="Gudang Garam",
    #                                  code="GG",
    #                                  address="Sunter Agung, Jakarta Utara, Indonesia.",
    #                                  description="Gudang Garam",
    #                                  custom_id="GG")
    # pprint.pprint(new_warehouse)

    pprint.pprint(get_sales_delivery('SO-000210').json())

    # pprint.pprint(new_contact.json())
    # # pprint.pprint(get_product_id_by_name('Microdot Executive Tray 703 H Black'))
    # # pprint.pprint(get_list_of_products())
    # transaction_nums = []
    # page = 1
    # purchase_orders = get_list_of_purchase_orders(page=page).json()
    # current_purchase_orders_page = int(purchase_orders['current_page'])
    # total_purchase_orders_page = int(purchase_orders['total_pages'])
    # while current_purchase_orders_page < total_purchase_orders_page:
    #     purchase_orders = get_list_of_purchase_orders(page=page).json()
    #     current_purchase_orders_page = int(purchase_orders['current_page'])
    #     total_purchase_orders_page = int(purchase_orders['total_pages'])
    #     for p in purchase_orders['purchase_orders']:
    #         transaction_nums.append(p.get('transaction_no'))
    #     page += 1
    # print(len(transaction_nums))
    # # pprint.pprint(create_product(name=product['name'],
    #                              product_code=product['catalog_code'],
    #                              buy_price=product['price'],
    #                              unit_name='Pcs',
    #                              sell_price=product['price']))
    #
    # for prod in get_list_of_products():
    #     if prod['product_name'] == product['name']:
    #         print(prod['product_id'])
    #         break
    #     # pprint.pprint(prod)
    # print("product not found")


    # # pprint.pprint(checking_purchase_orders(transaction_no='PO-000309'))
    # print(get_product_id_by_name(str('Swipe-All C86 Chemical Sorbent Sock 60 L (Penyerap Kimia), 12286')))
    # # pprint.pprint(purchase_orders[3])
    # # pprint.pprint(get_list_of_purchase_orders().json().get('purchase_orders'))
    # print(get_list_of_purchase_orders().json())