from sales import sales
from purchase import purchase
from main import read_sample_input
import pprint


if __name__ == "__main__":
    data = read_sample_input("sample_data.txt")[:4]
    purchase_order_date = "%s/%s/%s" % (data[0]['order_date'][8:10], data[0]['order_date'][5:7], data[0]['order_date'][:4])
    delivery_date = "%s/%s/%s" % (data[1]['delivery_date'][8:10], data[1]['delivery_date'][5:7], data[1]['delivery_date'][:4])
    invoice_date = "%s/%s/%s" % (data[2]['invoice_date'][8:10], data[2]['invoice_date'][5:7], data[2]['invoice_date'][:4])
    paid_date = "%s/%s/%s" % (data[3]['paid_at'][8:10], data[3]['paid_at'][5:7], data[3]['paid_at'][:4])
    # print("po date: %s" % purchase_order_date)
    # print("delivery date: %s" % delivery_date)
    # print("invoice date: %s" % invoice_date)
    # print("paid date: %s" % paid_date)
    # pprint.pprint(data)